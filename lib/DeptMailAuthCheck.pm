package DeptMailAuthCheck;

use Mo;
use Data::Dumper;
use DeptMail;

has 'reason';
has 'code';
has 'client_ip';
has 'client_auth';
has 'endpoint';
has 'params';
has 'config';
has 'loggers';
has 'dryrun';
has 'oidc_token';

sub is_allowed {
    my $self = shift;

    #
    # check input
    #
    for ( qw(client_ip client_auth endpoint params config loggers) ) {
        unless ( $self->{$_} ) {
            my $warning = "DeptMailAuthCheck::is_allowed missing '$_'";
            ( $self->loggers and $self->loggers->{error} ) ? $self->loggers->{error}($warning) : warn $warning;
            return 0;
        }
    }
    for ( qw(info debug error) ) {
        unless ( $self->loggers->{$_} ) {
            warn "DeptMailAuthCheck::is_allowed missing 'loggers->$_'";
            return 0;
        }
    }
    unless ( $self->{params}->{action} ) {
        $self->loggers->{error}("DeptMailAuthCheck::is_allowed missing 'params->action'");
        return 0;
    }

    #
    # get the auth_check configuration
    #
    my $auth_check = $self->config->{$self->{endpoint}}->{$self->params->{action}};
    unless ( $auth_check ) {
        $self->loggers->{debug}("no auth check config defined");
        return 0;
    }


    #
    # check auth IP
    # fails if 'ip' is defined in auth_check config
    # and the client_ip doesn't match
    #
    if ( $auth_check->{ip} and $self->client_ip ne $auth_check->{ip} ) {
        $self->loggers->{info}("auth check failed 'ip' check");
        return 0;
    }
    
    #
    # check auth IP list
    # fails if 'ip_list' is defined in auth_check config
    # and the client_ip doesn't match any of the list items
    #
    if ( $auth_check->{ip_list} ) {
        unless (grep $self->client_ip, $auth_check->{ip_list}) {
            $self->loggers->{info}("auth check failed 'ip_list' check");
            return 0;
        }
    }

    #
    # check auth IP RANGE
    # fails if 'ip_range' is defined in auth_check config
    # and the client_ip isn't in range
    #
    if ( $auth_check->{ip_range} and ! _ip_in_range($self->client_ip, $auth_check->{ip_range}) ) {
        $self->loggers->{info}("auth check failed 'ip_range' check");
        return 0;
    }

    #
    # check auth method
    # maps arguments to configuration-defined method
    # and fails based on the results of the method call
    #
    if ( $auth_check->{method} and $auth_check->{method_arg_map} ) {
        my $method = $auth_check->{method};
        if ( exists $self->config->{_always_pass_check_methods}->{$self->{endpoint}} and 
            grep( /$self->{params}->{action}/, @{$self->config->{_always_pass_check_methods}->{$self->{endpoint}}} ) ) {
                # Method is in config list for methods that always pass authcheck
                $self->loggers->{info}("auth check success: method '$self->{params}->{action}' always passes for '$self->{endpoint}'");
                return 1;
        } elsif ( ! $self->$method(
                _assemble_authcheck_method_args(
                    {
                        method_arg_map => $auth_check->{method_arg_map},
                        params         => $self->params,
                        client_ip      => $self->client_ip,
                        client_auth    => $self->client_auth,
                        dryrun         => $self->dryrun || undef,
                    },
                ),
                $auth_check->{disallow_if} || undef,
                $auth_check->{allow_only_if} || undef,
            )
        ) {
            $self->loggers->{info}("auth check failed: ".$auth_check->{method});
            return 0;
        }
    }

    #
    # If none of the checks failed, then the request is authorized
    #
    $self->loggers->{info}("auth check success");
    return 1;
}


#
# CHECK METHODS...
#

sub check_noop {
    my $self = shift;
    my $args = shift;
    $self->loggers->{debug}("check_noop ".Data::Dumper->new([$args])->Terse(1)->Indent(0)->Sortkeys(1)->Dump);
    return 1;
}

sub dm_authcheck {
    my $self = shift;
    my $args = shift;
    my $disallow_if = shift;
    my $allow_only_if = shift;
    my %config = %{$self->config->{_deptmail}};
    my $permissions = $self->config->{_dm_authcheck_permissions};
    my $dm = DeptMail->new(loggers => $self->loggers, %config);
    my $info_log = {
        auth_check  => (caller(0))[3],
        method      => $self->{params}->{action},
        agent       => $args->{agent},
        groups      => $args->{agent_groups},
        patient     => $args->{patient} || 'none',
        authorized  => 0,
        disallow_if => $disallow_if || undef,
        allow_only_if => $allow_only_if || undef,
    };
    if ( ! $dm->dm_authcheck(
            %config,
            _dm_authcheck_permissions => $permissions,
            method => $self->{params}->{action},
            agent => $args->{agent},
            agent_groups => $args->{agent_groups},
            patient => $args->{patient} || undef,
            disallow_if => $disallow_if || undef,
            allow_only_if => $allow_only_if || undef
        )
    ) {
        my $patient_name = 'on ' . $args->{patient} || 'with no context';
        $self->reason( $args->{agent} . " is not authorized to call " . $self->{params}->{action} . " $patient_name" );
        #$self->code(9000); # Codes?  Where we're going, we don't need codes!
        if ( ! $self->dryrun ) {
            $self->loggers->{info}(Data::Dumper->new([$info_log])->Terse(1)->Indent(0)->Dump);
        }
        return undef;
    } else {
        $info_log->{authorized} = 1;
        if ( ! $self->dryrun ) {
            $self->loggers->{info}(Data::Dumper->new([$info_log])->Terse(1)->Indent(0)->Dump);
        }
        return 1;
    }
}

sub oidc_authcheck {
    my $self = shift;
    my $args = shift;
    my $disallow_if = shift;
    my $allow_only_if = shift;
    my %config = %{$self->config->{_deptmail}};
    my $permissions = $self->config->{_dm_authcheck_permissions};
    my $dm = DeptMail->new(loggers => $self->loggers, %config);
    my $info_log = {
        auth_check  => (caller(0))[3],
        method      => $self->{params}->{action},
        patient     => $args->{patient} || 'none',
        authorized  => 0,
        disallow_if => $disallow_if || undef,
        allow_only_if => $allow_only_if || undef,
    };
    my $check_result = $dm->oidc_authcheck(
        %config,
        _dm_authcheck_permissions => $permissions,
        method => $self->{params}->{action},
        patient => $args->{patient} || undef,
        disallow_if => $disallow_if || undef,
        allow_only_if => $allow_only_if || undef,
        oidc_token  => $self->oidc_token || {},
    );
    if ( ! defined $check_result ) {
        # If we got an undefined response back, there was an issue with the token
        $self->reason( "Access token expired, revoked, malformed, or missing required information" );
        if ( ! $self->dryrun ) {
            $self->loggers->{info}(Data::Dumper->new([$info_log])->Terse(1)->Indent(0)->Dump);
        }
        $self->code(401);
        return undef;
    } elsif ( ! $check_result ) {
        # If we received a 0 back, the token was fine but authorization failed
        my $patient_name = 'on ' . $args->{patient} || 'with no context';
        $self->reason( "Token owner is not authorized to call " . $self->{params}->{action} . " $patient_name" );
        if ( ! $self->dryrun ) {
            $self->loggers->{info}(Data::Dumper->new([$info_log])->Terse(1)->Indent(0)->Dump);
        }
        $self->code(403);
        return undef;
    } else {
        # All checks passed; the call is authorized
        $info_log->{authorized} = 1;
        if ( ! $self->dryrun ) {
            $self->loggers->{info}(Data::Dumper->new([$info_log])->Terse(1)->Indent(0)->Dump);
        }
        return 1;
    }
}

sub domainadmin_authcheck {
    # Nearly identical to dm_authcheck, but it loads group memberships from a dynamic config file rather than requiring the user pass the groups
    my $self = shift;
    my $args = shift;
    my $disallow_if = shift;
    my $allow_only_if = shift;
    my %config = %{$self->config->{_deptmail}};
    my $permissions = $self->config->{_dm_authcheck_permissions};

    # Make sure the authenticated user has a list of groups in the dymaic config
    if ( ! exists $self->config->{_domainadmin_user_groups}->{$args->{agent}} ) {
        $self->reason( $args->{agent} . " is not a member of any administrative groups" );
        $self->code(403);
        return undef;
    }
    my $agent_groups = $self->config->{_domainadmin_user_groups}->{$args->{agent}};

    my $dm = DeptMail->new(loggers => $self->loggers, %config);
    my $info_log = {
        auth_check  => (caller(0))[3],
        method      => $self->{params}->{action},
        agent       => $args->{agent},
        groups      => $agent_groups,
        patient     => $args->{patient} || 'none',
        authorized  => 0,
        disallow_if => $disallow_if || undef,
        allow_only_if => $allow_only_if || undef,
    };
    if ( ! $dm->dm_authcheck(
        %config,
        _dm_authcheck_permissions => $permissions,
        method => $self->{params}->{action},
        agent => $args->{agent},
        agent_groups => $agent_groups,
        patient => $args->{patient} || undef,
        disallow_if => $disallow_if || undef
        )
    ) {
        my $patient_name = 'on ' . $args->{patient} || 'with no context';
        $self->reason( $args->{agent} . " is not authorized to call " . $self->{params}->{action} . " $patient_name" );
        $self->loggers->{info}(Data::Dumper->new([$info_log])->Terse(1)->Indent(0)->Dump);
        return undef;
    } else {
        $info_log->{authorized}= 1;
        $self->loggers->{info}(Data::Dumper->new([$info_log])->Terse(1)->Indent(0)->Dump);
        return 1;
    }
}

#
# UTILITY METHODS...
#

sub _ip_in_range {
    my ($ip, $range) = @_;
    # TODO not implemented
    return 0;
}

sub _assemble_authcheck_method_args {
    my $mapper = shift;
    return undef unless $mapper->{method_arg_map};
    my $mapped_args = undef;
    for my $key ( keys %{$mapper->{method_arg_map}} ) {
        if ( $mapper->{method_arg_map}->{$key} =~ /^params_(.*)/ ) {
            $mapped_args->{$key} = $mapper->{params}->{$1};
        }
        else {
            $mapped_args->{$key} = $mapper->{$mapper->{method_arg_map}->{$key}};
        }
    }
    return $mapped_args;
}


1;
