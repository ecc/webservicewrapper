use strict;
use warnings;
package WebServiceWrapper 1.4;

# ABSTRACT: Provides a simple versatile web service that implements your class in a straightforward way

use Dancer;
use Dancer::Plugin::REST;
use Dancer::Plugin::Auth::Basic;
use Dancer::Plugin::DynamicConfig;
use Class::Load qw/:all/;
use Try::Tiny;
use IO::Capture::Stderr;
use Data::Dump qw(dump dumpf);
use Digest::HMAC_SHA1;
use MIME::Base64;
use Socket;

# mega debugging
#hook before => sub {
#    info dump_string(request());
#};

# interpret format in the file extention (json, xml, yaml)
prepare_serializer_for_format;

any '/:method.:format' => sub {

    my $params = params;

    # this is the method that will be called in the wrapper class
    my $method = $params->{method};
    my $run_method;
    my $authuser = get_user(request) || "no_auth_user";
    my $authcheck_dryrun = $params->{authcheck_dryrun};
    my $disallow_if = $params->{disallow_if} || [];

    # since we will be passing the POST params back to the method,
    # we will need to remove these params first
    delete $params->{method};
    delete $params->{format};
    delete $params->{authcheck_dryrun};

    #
    # IP authorization
    # for access to the entire web service
    # (all URIs/endpoints/actions)
    #
    # Note that it doesn't check IP authz specifically for the
    # endpoint and/or action being called.
    #
    # Those checks can be done using the authcheck callout; the IP is passed in
    # to the class to be used by the is_allowed() method that you can implement
    # however is suitable.
    #
    my $ip = request->remote_address || 'unknown_ip';
    my $allowed_hosts = config->{allowed_hosts};

    if ( $ip and $allowed_hosts and ! grep /^$ip$/, @{$allowed_hosts} ) {
        info "FAIL AUTHUSER=$authuser ENDPOINT=$method unauthorized ip $ip";
        # the status_### methods only seem to be available in Dancer2, which we aren't using, so this is causing Dancer to crash
        # switching to the error method supported by Dancer (the original)
        #return status_403( { result => "unauthorized ip" } );
        return status_bad_request( { result => "unauthorized ip" } );
    }

    # Maintenance/unavailable override
    if ( config->{maintenance} and ref config->{maintenance} eq 'ARRAY' ) {
        if ( grep /^$method$/, @{config->{maintenance}} or grep( /^all$/, @{config->{maintenance}} ) ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method route in maintenance mode";
            return send_entity( 'Service temporarily unavailable', 503 );
        }
    }

    #
    # Victorops-style signature-based authentication
    #
    if ( config->{authsignature} && config->{authsignature}->{$method} ) {
        unless (
            config->{authsignature}->{$method}->{key}
                and
            config->{authsignature}->{$method}->{hdr}
                and
            config->{authsignature}->{$method}->{str}
        ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method IP=$ip authsignature config needs: key,hdr,str";
            return status_bad_request( { result => "server configuration error" } );
        }
        my $signature_check = check_signature(
            request,
            config->{authsignature}->{$method}->{key},
            config->{authsignature}->{$method}->{hdr},
            config->{authsignature}->{$method}->{str},
        );
        if ( ! $signature_check ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method IP=$ip failed authsignature";
            return status_bad_request( { result => "failed authsignature" } );
        }
    }

    #
    # Header token-based authentication
    #
    if ( config->{headertoken} && config->{headertoken}->{$method} ) {
        unless (
            config->{headertoken}->{$method}->{hdr}
                and
            config->{headertoken}->{$method}->{tkn}
        ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method IP=$ip authsignature config needs: hdr,tkn";
            return status_bad_request( { result => "server configuration error" } );
        }
        my $token_check = check_token(
            request,
            config->{headertoken}->{$method}->{hdr},
            config->{headertoken}->{$method}->{tkn},
        );
        if ( ! $token_check ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method IP=$ip failed authtoken";
            return status_bad_request( { result => "failed authtoken" } );
        }
    }

    #
    # OIDC Access Tokenmay be present; will be validated in authorization check method
    #
    my $oidc_token = {};
    if ( config->{oidctoken} && config->{oidctoken}->{$method} ) {
        unless (
            config->{oidctoken}->{$method}->{hdr}
                and
            config->{oidctoken}->{$method}->{type}
        ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method IP=$ip oidctoken config needs: hdr,type";
            return status_bad_request( { result => "server configuration error" } );
        }
        my $header_val = get_header_val(
            request,
            config->{oidctoken}->{$method}->{hdr},
        );
        if ( $header_val ) {
            $oidc_token = { token => $header_val, type => config->{oidctoken}->{$method}->{type} };
        }
    }

    #
    # get a string dump of the params for logging purposes
    #
    my @nologparams = ();
    if ( config->{wrapper_nologparams} ) {
        push @nologparams, @{config->{wrapper_nologparams}->{_all_methods}} if config->{wrapper_nologparams}->{_all_methods};
        push @nologparams, @{config->{wrapper_nologparams}->{$method}} if config->{wrapper_nologparams}->{$method};
    }
    my $param_dump = scalar @nologparams ?
                         dump_string($params, sub {{ hide_keys => [ @nologparams ]}}) :
                         dump_string($params);

    #
    # make sure that the configuration has the wrapper class defined
    #
    my $wrapper_class = config->{wrapper_class};
    unless ( $wrapper_class ) {
        error "FAIL AUTHUSER=$authuser ENDPOINT=$method wrapper_class not defined";
        return status_bad_request( { result => "internal error (1)" } );
    }

    #
    # try to load the wrapper class
    #
    my ( $load_ok, $code ) = try_load_class($wrapper_class);
    unless ( $load_ok ) {
        error "FAIL AUTHUSER=$authuser ENDPOINT=$method unable to load $wrapper_class: $code";
        return status_bad_request( { result => "internal error (2)" } );
    }

    #
    # load any args from the config that need to be passed into the constructor
    #
    my $constructor_extargs = undef;
    my $i = '';
    while ( 1 ) {
        last unless ( config->{plugins}->{DynamicConfig}->{"_constructor_extargs$i"} );
        my $_constructor_extargs = dynamic_config("_constructor_extargs$i");
        unless ( $_constructor_extargs and ref $_constructor_extargs eq 'HASH' ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method unable to load constructor_extargs$i config (_constructor_extargs$i)";
            return status_bad_request( { result => "unable to load constructor_extargs$i config (_constructor_extargs$i)" } );
        }
        my $constructor_extargs_passref = config->{'wrapper_constructor_extargs'.$i.'_passref'};
        if ( $constructor_extargs_passref ) {
            $constructor_extargs->{$constructor_extargs_passref} = $_constructor_extargs;
        }
        else {
            if ( $i ) {
                info "FAIL AUTHUSER=$authuser ENDPOINT=$method must passref constructor_extargs$i config (_constructor_extargs2)";
                return status_bad_request( { result => "must passref constructor_extargs$i config (_constructor_extargs2)" } );
            }
            else {
                $constructor_extargs = $_constructor_extargs;
            }
        }
        $i++;
    }

    #
    # optionally pass additional context to the class constructor
    #
    if ( config->{'wrapper_constructor_servercontext'} ) {
        $constructor_extargs->{config->{'wrapper_constructor_servercontext'}} = {
            client_ip   => $ip,
            client_auth => $authuser,
            endpoint    => $method,
        };
        # If oidc_token has keys, pass that along with the context
        if ( $oidc_token && ref( $oidc_token ) && ref( $oidc_token ) eq 'HASH' && scalar keys %{ $oidc_token } ) {
            $constructor_extargs->{config->{'wrapper_constructor_servercontext'}}->{oidc_token} = $oidc_token;
        }
    }

    #
    # instantiate the wrapper class
    #
    my $wrapper;
    try {
        $wrapper = $wrapper_class->new(%$constructor_extargs)
    } catch {
        error "FAIL AUTHUSER=$authuser ENDPOINT=$method problem with $wrapper_class: $_";
    }
    or return status_bad_request( { result => "internal error (3)" } );

    #
    # pass the dancer logger in so that downstream code can log
    #
    try {
        $wrapper->loggers( {
            info    => \&Dancer::Logger::info,
            error   => \&Dancer::Logger::error,
            debug   => \&Dancer::Logger::debug,
            core    => \&Dancer::Logger::core,
            warning => \&Dancer::Logger::warning,
        } );
    } catch {
        error "FAIL AUTHUSER=$authuser ENDPOINT=$method unable to add loggers to $wrapper_class: $_";
    };

    #
    # The endpoint uri --> class method
    #     ($method)    --> ($run_method)
    # can be defined in the configuration in 2 ways
    #
    # 1) explicitely defined in the config so that foo.json executes the foo()
    # method in the wrapper class
    #
    # e.g.
    #
    #   wrapper_methods:
    #      - foo
    #      - bar
    #
    if ( ref config->{wrapper_methods} eq 'ARRAY' ) {
        if ( grep /^$method$/, @{config->{wrapper_methods}} ) {
            $run_method = $method;
        }
    }
    #
    # 2) or by means of abstraction
    #
    # e.g.
    #
    #   wrapper_methods:
    #      service1: foo
    #      service2: bar
    #      service3: foo
    #
    elsif ( ref config->{wrapper_methods} eq 'HASH' ) {
        $run_method = config->{wrapper_methods}->{$method};
    }
    unless ( $run_method ) {
        error "FAIL AUTHUSER=$authuser ENDPOINT=$method unregistered method $method in $wrapper_class";
        return status_bad_request( { result => "invalid method ($method)" } );
    }

    #
    # load dynamic configuration for actionchecks
    #
    # The 'action' POST parameter is a special parameter that can be used by clients
    # to call the wrapper class method with more complexity.
    #
    # For example, the wrapper class might be just invoking methods in
    # an existing class (no need to rewrite code in your wrapper)
    #
    # e.g.
    #
    #   wrapper_class is UtilWrapper
    #   run_method is foo
    #
    #   package UtilWrapper;
    #   ...
    #   sub foo {
    #     my $action = $params->{action};
    #     my $util = Util->new(...);
    #     my $dataout = $util->$action(
    #        %{$params},
    #     );
    #   }
    #
    # so...
    #
    # The point of the actioncheck configuration is to provide a layer of
    # security and transparency around this powerful ability for clients to call
    # methods directly in these downstream methods
    #
    # e.g.
    #
    #   if the method (web service endpoint)
    #   is https://.../foo.json
    #
    #   and in the configuration
    #
    #       DynamicConfig:
    #           'foo_actioncheck': '/path/to/foo.json'
    #
    #   then foo.json should be a JSON formatted file of the form
    #
    #      {
    #         "action1" : {
    #             "param1" : "1",
    #             "param2" : "optional"
    #          },
    #         "action2" : {
    #             ".authcheck" : "AuthCheckClass"
    #             "param4" : "1",
    #             "param5" : "1"
    #          }
    #      }
    #
    # more details inline...
    #
    if ( config->{plugins}->{DynamicConfig}->{$method."_actioncheck"} ) {
        my $method_action_config = dynamic_config($method."_actioncheck");
        unless ( $method_action_config ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method unable to load actioncheck config ($method.json)";
            return status_bad_request( { result => "unable to load actioncheck config ($method.json)" } );
        }

        #
        # since actionchecking is a layer of security, we want to fail the
        # connection if the client is specifying an action that isn't explicity
        # defined
        #
        # we would not want clients to be making requests to arbitrary methods
        # in downstream code!
        #
        my $action = $params->{action};
        unless ( exists $method_action_config->{$action} and ref($method_action_config->{$action}) eq 'HASH' ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method bad action ($action) given for wrapper method ($method)";
            return status_bad_request( { result => "bad action ($action) given for wrapper method ($method)" } );
        }

        #
        # simularly, for security, we do not want extraneous parameters being
        # passed back to the downstream code
        #
        for my $arg ( keys %{$params} ) {
            next if $arg eq 'action';
            unless ( defined $method_action_config->{$action}->{$arg} ) {
                delete $params->{$arg};
                info("WARNING AUTHUSER=$authuser ENDPOINT=$method deleted extraneous arg ($arg) for action ($action) in wrapper method ($method)");
            }
        }

        #
        # any args (POST parameters) that are marked as 1 or 'required' in the
        # actioncheck config are required
        #
        # the request will fail if they are missing
        #
        # any other value (such as 'optional') means that the arg is optional
        #
        for my $arg ( keys %{$method_action_config->{$action}} ) {
            if ( $method_action_config->{$action}->{$arg} =~ /^(1|required)$/ and ! exists $params->{$arg} ) {
                info "FAIL AUTHUSER=$authuser ENDPOINT=$method missing arg ($arg) for action ($action) in wrapper method ($method)";
                return status_bad_request( { result => "missing arg ($arg) for action ($action) in wrapper method ($method)" } );
            }
        }

        #
        # authcheck callout
        #
        # for an additional layer of security
        # each action you may specify a class
        # using the special .authcheck option
        # in the actioncheck configuration
        #
        # e.g.
        #
        # {
        #     'action1' : {
        #        '.authcheck' : 'MyAuthCheckClass'
        #
        # to check/enforce robust authorization using as input ...
        #   client_ip
        #   client_authuser
        #   endpoint (method)
        #   params and their values
        #
        # essentially the authcheck class only needs to implement the
        # is_allowed() method and return true or false based on input
        #
        my $authcheck_class = $method_action_config->{$action}->{'.authcheck'};
        if ( $authcheck_class ) {

            #
            # try to load the authcheck class
            #
            my ( $load_ok, $code ) = try_load_class($authcheck_class);
            unless ( $load_ok ) {
                error "FAIL AUTHUSER=$authuser ENDPOINT=$method unable to load $authcheck_class: $code";
                return status_bad_request( { result => "internal error (4)" } );
            }

            #
            # you can also specify a configuration file for your authcheck class
            # which is dynamically loaded from the file defined at
            #
            # e.g.
            #   plugins:
            #     DynamicConfig:
            #         MyAuthCheckClass_authcheck: '/path/to/authcheckconf.json'
            #
            #
            my $authcheck_config = undef;
            if ( config->{plugins}->{DynamicConfig}->{$authcheck_class."_authcheck"} ) {
                $authcheck_config = dynamic_config($authcheck_class."_authcheck");
            }

            #
            # instantiate the authcheck class
            #
            my $authcheck;
            try {
                $authcheck = $authcheck_class->new(
                    loggers => {
                        info    => \&Dancer::Logger::info,
                        error   => \&Dancer::Logger::error,
                        debug   => \&Dancer::Logger::debug,
                        core    => \&Dancer::Logger::core,
                        warning => \&Dancer::Logger::warning,
                    },
                    config      => $authcheck_config,
                    client_ip   => $ip,
                    client_auth => $authuser,
                    endpoint    => $method,
                    oidc_token  => $oidc_token,
                    params      => $params,
                    dryrun      => $authcheck_dryrun || undef,
                    disallow_if => $disallow_if || [],
                )
            } catch {
                error "FAIL AUTHUSER=$authuser ENDPOINT=$method problem with $authcheck_class: $_";
            }
                or return status_bad_request( { result => "internal error (5)" } );

            # here we check to see
            # if the authcheck class allows or
            # disallows this request
            unless ( $authcheck->is_allowed ) {
                info "FAIL AUTHUSER=$authuser ENDPOINT=$method authcheck failed: ".$authcheck->reason;
                return status_bad_request( { result => "authcheck failed", code => $authcheck->code } );
            }
            if ( $authcheck_dryrun ) {
                info "INFO AUTHUSER=$authuser ENDPOINT=$method dryrun authcheck succeeded, method not called";
                my $return_stuff = undef;
                $return_stuff->{result} = 1;
                return status_ok( $return_stuff );
            }
        }

    }

    #
    # extra args can be passed in from the config and included with each request
    # to the run_method
    #
    my $extargs = config->{wrapper_extargs};
    if ( $extargs ) {
        if ( ref $extargs eq 'HASH' ) {

            # e.g.
            #
            # wrapper_extargs: {
            #   _all_methods: {
            #     env: 'prod'
            #   }
            #
            if ( $extargs->{_all_methods} ) {
                if ( ref $extargs->{_all_methods} eq 'HASH' ) {
                    for my $arg ( keys %{$extargs->{_all_methods}} ) {
                        $params->{$arg} = $extargs->{_all_methods}->{$arg};
                    }
                }
                else {
                    error "WARNING AUTHUSER=$authuser ENDPOINT=$method wrapper_extargs: '_all_methods' bad format";
                }
            }

            # e.g.
            #
            # wrapper_extargs: {
            #   mymethod: {
            #     conf: '/path/to/file'
            #   }
            #
            if ( $extargs->{$method} ) {
                if ( ref $extargs->{$method} eq 'HASH' ) {
                    for my $arg ( keys %{$extargs->{$method}} ) {
                        $params->{$arg} = $extargs->{$method}->{$arg};
                    }
                }
                else {
                    error "wrapper_extargs: '$method' bad format";
                }
            }
        }
        else {
            error "wrapper_extargs bad format";
        }
    }


    #
    # extargs can also be passed in via dynamic configuration
    #
    # 1) for all methods
    #
    if ( config->{plugins}->{DynamicConfig}->{"_all_methods_extargs"} ) {
        $extargs = dynamic_config("_all_methods_extargs");
        unless ( $extargs ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method  unable to load extargs config (_all_methods_extargs)";
            return status_bad_request( { result => "unable to load extargs config (_all_methods_extargs)" } );
        }
        for my $arg ( keys %{$extargs} ) {
            $params->{$arg} = $extargs->{$arg};
        }
    }
    #
    # 2) or for individual methods
    #
    if ( config->{plugins}->{DynamicConfig}->{$method."_extargs"} ) {
        $extargs = dynamic_config($method."_extargs");
        unless ( $extargs ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method unable to load extargs config ($method"."_extargs)";
            return status_bad_request( { result => "unable to load extargs config ($method"."_extargs)" } );
        }
        for my $arg ( keys %{$extargs} ) {
            $params->{$arg} = $extargs->{$arg};
        }
    }

    # param debug
    my $param_dump_debug = dump_string($params);

    # capture warn()'s generated from wrapper class, unless it's a nocapturestderr method
    my $capture;
    my $capture_stderr = 1;
    if ( config->{wrapper_no_capture_stderr_methods} ) {
        if (
            ( config->{wrapper_no_capture_stderr_methods}->{_all_methods} and grep( /^$params->{action}$/, @{config->{wrapper_no_capture_stderr_methods}->{_all_methods}} ) )
                or
            ( config->{wrapper_no_capture_stderr_methods}->{$method} and grep( /^$params->{action}$/, @{config->{wrapper_no_capture_stderr_methods}->{$method}} ) )
        ) {
            $capture_stderr = 0;
        }
    }
    if ( $capture_stderr ) {
        $capture = IO::Capture::Stderr->new();
        $capture->start();
    }

    #
    # here is where we make the call to the run_method in the wrapper_class
    #
    try {
        $wrapper->$run_method($params)
    } catch {
        # Capture the error and strip newlines
        my $err = $_; $err = '' unless $err; $err =~ s/(?:\r?\n)+/ /g;
        if ( $capture_stderr ) {
            error "problem running method $run_method ($method) in $wrapper_class: $err".dump_string([$capture->read]);
        } else {
            error "problem running method $run_method ($method) in $wrapper_class: $err";
        }
        debug "params given to $run_method ($method) in $wrapper_class: ", $param_dump_debug;
    }
    or return status_bad_request( { result => "failed to run method $run_method ($method) in $wrapper_class" } );

    # stop capturing warn()'s
    if ( $capture_stderr ) {
        $capture->stop();
    }

    # process the results
    my $result      = $wrapper->result();

    # get the code and message
    # wrap this in an eval{} since those methods might not be implemented
    my $result_code = eval { $wrapper->result_code() } || undef;
    my $result_mesg = eval { $wrapper->result_mesg() } || undef;

    my @nologresult = ();
    if ( config->{wrapper_nologresult} ) {
        push @nologresult, @{config->{wrapper_nologresult}->{_all_methods}} if config->{wrapper_nologresult}->{_all_methods};
        push @nologresult, @{config->{wrapper_nologresult}->{$method}}      if config->{wrapper_nologresult}->{$method};
    }
    my $result_dump = scalar @nologresult ?
                             dump_string($result, sub {{ hide_keys => [ @nologresult ]}}) :
                             dump_string($result);
    $result_dump = "not_shown" if ( grep /^$params->{action}$/, @nologresult );
    $result_dump = "not_shown" if ( grep /^_all_results$/, @nologresult );

    # truncate results
    my ($truncate)  = grep /^$params->{action}:truncate/, @nologresult;
    if ( ! $truncate ) {
        ($truncate) = grep /^_all_results:truncate/, @nologresult;
    }
    if ( $truncate ) {
        my ($truncate_at) = $truncate =~ /truncate:(\d+)$/ if $truncate;
        if ( length($result_dump) >= $truncate_at ) {
            my $trlen = length($result_dump) - $truncate_at;
            $result_dump = substr($result_dump, 0, $truncate_at)."(truncated $trlen characters)";
        }
    }


    # return the results
    if ( $wrapper->status() ) {

        my $error_capture_str;
        if ( $capture_stderr ) {
            $error_capture_str = dump_string([$capture->read]);
        }
        info sprintf(
            "OK AUTHUSER=$authuser ENDPOINT=$method RESULT=%s %s%s%sPARAMS=%s",
            $result_dump,
            defined $result_code ? "CODE=$result_code " : "",
            defined $result_mesg ? "MESG=$result_mesg " : "",
            defined $error_capture_str ? "ERROR=$error_capture_str " : "",
            $param_dump ? $param_dump : "NONE",
        );
        debug "params given to $run_method ($method) in $wrapper_class: ", $param_dump_debug;
        if ( config->{do_not_encode_result} ) {
            return status_ok( $result );
        }
        my $return_stuff = undef;
        $return_stuff->{result} = $result;
        $return_stuff->{code}   = $result_code if defined $result_code;
        $return_stuff->{mesg}   = $result_mesg if defined $result_mesg;
        return status_ok( $return_stuff );

    } else {

        if ( $capture_stderr ) {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method RESULT=$result_dump ERROR=".dump_string([$capture->read])." PARAMS=", $param_dump;
        } else {
            info "FAIL AUTHUSER=$authuser ENDPOINT=$method RESULT=$result_dump PARAMS=", $param_dump;
        }
        debug "params given to $run_method ($method) in $wrapper_class: ", $param_dump_debug;
        return status_bad_request( { result => $result } );

    }
};

sub dump_string {
    my ($obj, $filter) = @_;
    my $string =  dumpf($obj, $filter);
       $string =~ s/^\s*#.*//mg;
       $string =~ s/^\s+//mg;
       $string =~ s/(?:\r?\n)+/ /g;
    return $string;
}

sub get_user {
    my $req = shift;
    use MIME::Base64;
    my $auth = $req->env->{HTTP_AUTHORIZATION};
    if (defined $auth && $auth =~ /^Basic (.*)$/) {
        my ($user, $password) = split(/:/, (MIME::Base64::decode($1) || ":"));
        return $user;
    }
}

sub check_signature {
    my ($req, $sigkeys, $sighdr, $sigstr) = @_;

    # Authenticating Webhook Requests
    # https://help.victorops.com/knowledge-base/escalation-webhooks/
    # In order to authenticate that the POST requests are arriving to your application from VictorOps complete these steps:

    # 1. Create a string with the URL of the webhook, exactly how it appears in VictorOps; this includes trailing slashes etc…
    my $string = $sigstr;
    my %params = $req->params();
    for my $key ( keys %params ) {
        if ( $string =~ /$key=/ ) {
            $string =~ s/$key=\*/$key=$params{$key}/;  # the * is used to allow for variable URL embedded params
            delete $params{$key};                      # delete the param for steps 2/3
        }
    }

    # 2. Sort the request’s POST variables alphabetically by key.
    # 3. Append each POST variable’s key and value to the URL string, with no delimiter.
    my $poststring = '';
    for my $key ( sort keys %params ) {
        $poststring .= $key.$params{$key};
    }
    $string .= $poststring;

    # ... our hack to support multiple webhook/keys per web svc endpoint
    for my $sigkey ( split ',', $sigkeys ) {

        # 4. Hash the resulting string with HMAC-SHA1, using the webhook’s authentication key to generate a binary signature.
        my $hmac = Digest::HMAC_SHA1->new($sigkey);
        $hmac->add($string);

        # 5. Base64 encode the signature
        my $digest = $hmac->digest;

        # 6. Compare the output with the key X-VictorOps-Signature in the request
        # – if it matches, the request originated from VictorOps.
        my $hdr = $req->header($sighdr);
        if ( $hdr and decode_base64($hdr) eq $digest ) {
            return 1;
        }
    }
    return 0;
}

sub check_token {
    my ($req, $hdr, $tkn) = @_;

    # Authenticating Requests with authentication header tokens
    # https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#secret-token

    my $tknval = $req->header($hdr) || undef;
    if ( $tknval and $tknval eq $tkn ) {
        return 1;
    }
    return 0;
}

sub get_header_val {
    my ($req, $hdr) = @_;

    my $hdrval = $req->header($hdr) || undef;

    return $hdrval;
}


1;
