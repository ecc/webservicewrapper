use strict;
use warnings;

package DeptMailWrapper;

use Mo;
use Data::Dumper;
use DeptMail;

has 'result' => (is => 'rw');
has 'status' => (is => 'rw', isa => 'Bool');
has 'loggers' => (is => 'rw');
has 'result_code' => (is => 'rw');
has 'result_mesg' => (is => 'rw');
has 'config';
has 'authcheckconfig';
has 'servercontext';

sub amsutil {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub wisclist {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub wiscadmin {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub powershell {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub victorops {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub pspeer {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub calext_rpc {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub myuw {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub wmutil {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub itsm {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub healthcheck {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub dockerhealthcheck {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub google {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub domainadmin {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub helpdesk {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub msgq {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub relay {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub compromised {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub kitchensync {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub starlord {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub tangina {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub tangerine {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub webex {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub doubleagent {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub o365status {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub aaa {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub listsync {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub zoomconfig {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub uac {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub dataslayeralert {
    my $self = shift;
    my $params = shift;
    return $self->_deptmail($params);
}

sub _deptmail {
    my $self = shift;
    my $params = shift;

    if ( $params and ref($params) eq 'HASH' ) {

        unless ( keys %{$params} ) {
            $self->status(0);
            $self->result("no params given");
            return 1;
        }

        my $action = $params->{action};
        unless ( $action ) {
            $self->status(0);
            $self->result("bad params given");
            return 1;
        }

        # make the call to DeptMail and get the result
        my $dm = DeptMail->new(
            loggers => $self->loggers,
            %{ $self->config },
            authcheckconfig => $self->authcheckconfig,
            servercontext   => $self->servercontext,
        );
        my $dataout = $dm->$action(
            %{$params},
        );
        $dm->close_ldap();

        $self->status(1);
        $self->result( $dataout );
        $self->result_code($dm->result_code());
        $self->result_mesg($dm->result_mesg());
        return 1;

    }
    else {
        $self->status(1);
        $self->result("no params");
        return 1;
    }
}

1;
