package DummyClass;

use Mo;
use Data::Dumper;

has 'result' => (is => 'rw', isa => 'Str');
has 'status' => (is => 'rw', isa => 'Bool');
has 'loggers';
has 'config';
has 'config1';
has 'servercontext';

sub run {
    my $self = shift;
    my $params = shift;

    if ( $params and ref($params) eq 'HASH' ) {
        unless ( keys %{$params} ) {
            $self->status(0);
            $self->result("no params given");
            return 1;
        }
        $self->status(1);
        my $result_text = "ref is " . ref($params) .  " dump: "   . Data::Dumper->new([$params])->Terse(1)->Indent(0)->Sortkeys(1)->Dump;
        $result_text .= " config: " . Data::Dumper->new([$self->config])->Terse(1)->Indent(0)->Sortkeys(1)->Dump if $self->config;
        $result_text .= " config1: " . Data::Dumper->new([$self->config1])->Terse(1)->Indent(0)->Sortkeys(1)->Dump if $self->config1;
        $result_text .= " servercontext " . Data::Dumper->new([$self->servercontext])->Terse(1)->Indent(0)->Sortkeys(1)->Dump if $self->servercontext;
        $self->result($result_text);
        return 1;
    }
    else {
        $self->status(1);
        $self->result("no params");
        return 1;
    }
}

sub run_error {
    warn "DummyClass threw a warning";
}

sub run_array {
    my $self = shift;
    my $params = shift;

    if ( $params and $params->{args} ) {
        if ( ref $params->{args} eq 'ARRAY' ) {
            $self->status(1);
            $self->result(join(',',@{$params->{args}}));
            return 1;
        }
        elsif ( ! ref $params->{args} ) {
            $self->status(1);
            $self->result($params->{args});
            return 1;
        }
    }

    $self->status(0);
    $self->result("bad params dump: ".Data::Dumper->new([$params])->Terse(1)->Indent(0)->Sortkeys(1)->Dump);
    return 1;
}

sub run_hash {
    my $self = shift;
    my $params = shift;

    $self->status(1);
    $self->result($params);
    return 1;
}

1;
