#!/usr/bin/env perl
use lib './WebService/lib';
use lib './DeptMail/lib';
use feature ':5.10';
use strict;
use warnings;
use Test::More tests => 1;

use DeptMailAuthCheck;

is_deeply(
    DeptMailAuthCheck::_assemble_authcheck_method_args({
        method_arg_map => {
            gravity  => 'client_auth',
            einstein => 'params_e',
        },
        params         => {
            action => 'reaction',
            e      => 'mcsquared',
        },
        client_ip      => '1.2.3.4',
        client_auth    => 'newton',
    }),
    {
        einstein => 'mcsquared',
        gravity  => 'newton',
    }
);


