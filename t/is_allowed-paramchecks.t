#!/usr/bin/env perl
use lib './WebService/lib';
use lib './DeptMail/lib';
use feature ':5.10';
use strict;
use warnings;
use Test::More tests => 10;
use Test::Warn;
use DeptMailAuthCheck;

my $log_warn = sub { warn @_ };

warnings_are
    {
        DeptMailAuthCheck->new(
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'loggers'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'config'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'params'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'client_ip'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'client_auth'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'endpoint'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'params->action'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'loggers->error'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'loggers->debug'"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "DeptMailAuthCheck::is_allowed missing 'loggers->info'"
    ];
