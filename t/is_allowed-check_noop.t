#!/usr/bin/env perl
use lib './WebService/lib';
use lib './DeptMail/lib';
use feature ':5.10';
use strict;
use warnings;
use Test::More tests => 2;
use Test::Warn;
use DeptMailAuthCheck;

my $log_warn = sub { warn @_ };

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'params_admin',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "check_noop {'address' => 'foo\@wisc.edu','admin' => 'bbadger'}",
        "auth check success"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                        method         => 'check_noop',
                        method_arg_map => {
                            address => 'params_address',
                            admin   => 'client_auth',
                        }
                    },
                }
            },
            params => {
                action   => 'noop',
                address  => 'foo@wisc.edu',
                admin    => 'bbadger',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "check_noop {'address' => 'foo\@wisc.edu','admin' => 'wiscadmin'}",
        "auth check success"
    ];

