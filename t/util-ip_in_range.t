#!/usr/bin/env perl
use lib './WebService/lib';
use lib './DeptMail/lib';
use feature ':5.10';
use strict;
use warnings;
use Test::More tests => 1;

use DeptMailAuthCheck;

ok(! DeptMailAuthCheck::_ip_in_range('1.2.3.4', 'foo'));

#ok( DeptMailAuthCheck::_ip_in_range('1.2.3.4', '1.2.0.0/16'));

