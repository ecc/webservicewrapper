#!/usr/bin/env perl
use lib './WebService/lib';
use lib './DeptMail/lib';
use feature ':5.10';
use strict;
use warnings;
use Test::More tests => 1;
use Test::Warn;
use DeptMailAuthCheck;

my $log_warn = sub { warn @_ };
my $dmac     = DeptMailAuthCheck->new( loggers => { info => $log_warn, debug => $log_warn } );

warning_is
    {
        $dmac->check_noop(
            DeptMailAuthCheck::_assemble_authcheck_method_args(
                {
                    method_arg_map => {
                        admin    => 'params_admin',
                        address  => 'params_address',
                    },
                    params => {
                        address  => 'foo@wisc.edu',
                        admin    => 'bbadger',
                    },
                    client_ip    => '1.2.3.4',
                    client_auth  => 'wiscadmin',
                    endpoint     => 'wiscadmin',
                }
            )
        )
    } "check_noop {'address' => 'foo\@wisc.edu','admin' => 'bbadger'}";

