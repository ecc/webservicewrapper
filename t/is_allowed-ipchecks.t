#!/usr/bin/env perl
use lib './WebService/lib';
use lib './DeptMail/lib';
use feature ':5.10';
use strict;
use warnings;
use Test::More tests => 2;
use Test::Warn;
use DeptMailAuthCheck;

my $log_warn = sub { warn @_ };

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.5',
                    },
                }
            },
            params => {
                action   => 'noop',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "auth check failed 'ip' check"
    ];

warnings_are
    {
        DeptMailAuthCheck->new(
            loggers => { info => $log_warn, debug => $log_warn, error => $log_warn },
            config  => {
                wiscadmin => {
                    noop => {
                        ip             => '1.2.3.4',
                    },
                }
            },
            params => {
                action   => 'noop',
            },
            client_ip    => '1.2.3.4',
            client_auth  => 'wiscadmin',
            endpoint     => 'wiscadmin',
        )
        ->is_allowed
    } [
        "auth check success"
    ];

